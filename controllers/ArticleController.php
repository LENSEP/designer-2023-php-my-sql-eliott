<?php

class ArticleController{

    public static function index(){
        $articles = new Article();
        return[
            'articles' => $articles->getArticles(),
            'title'=> 'Blog',
            'corps' => 'Vues/pages/blog.php'
        ];
    }
    public static function adminIndex(){
        $articles= new Article();
      

        return[
            'articles'=> $articles->getArticles(),
            'title'=> 'Liste des articles',
            'corps'=>'vues/admin/indexArticle.php'

        ];
    }
    public static function show(){
        if(isset($_GET['id']) && $_GET['id']!=NULL){
            $id= $_GET['id'];
            $article = new Article() ;
            if($article->getArticle($id)==false){
                header('Location: index.php?route=404');
            }
                
        
        return [
            'article'=> $article->getArticle($id),
            'title' => $article->titre,
            'corps' => 'vues/pages/article.php',
        ];
    } else {
        header('location: index.php?route=404');
    }


    }
    public static function create(){

        return [
            'title'=> 'Ajouter un article',
            'corps'=>  'vues/admin/createArticle.php'
        ];

    }

    public static function store($data){
        $article = new Article();
        $article->titre = $data['titre'];
        $article->date=$data['date'];
        $article->auteur=$data['auteur'];
        $article->contenu=$data['contenu'];
        $article->image=$data['image'];
        $article->saveArticle();

        $_SESSION['alert'] = [
            'type' => 'success',
            'message' => 'Article enregistré avec succès'        
            
        ];

        header('Location: index.php?route=blog');
    }

    public static function edit($id){
        $article= new Article();
        $dataArticle = $article->getArticle($id);
        return [
            'article'=>$dataArticle,
            'corps' =>'vues/admin/editArticle.php',
            'title' => 'Modifier l\'article '.$dataArticle['titre']
            
        ];
    }

    public static function update($id, $data){
        $article = new Article();
        $article->id=$id;
        $article->titre = $data['titre'];
        $article->date=$data['date'];
        $article->auteur=$data['auteur'];
        $article->contenu=$data['contenu'];
        $article->image=$data['image'];
        $article->editArticle();

        $_SESSION['alert'] = [
            'type'=>'warning',
            'message'=>'Article modifié avec succès'
        ];
        header('Location: index.php?route=blog');
    }

    public static function delete($id){
        $article=new Article();
        $article->id=$id;
        $article->deleteArticle();

        $_SESSION['alert']=[
            'type'=>'danger',
            'message'=>'Article supprimé avec succès'
        ];

        header('Location: index.php?route=blog');
    }


}