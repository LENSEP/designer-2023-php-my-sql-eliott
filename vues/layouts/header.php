<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="Assets/css/main.css">
    <title><?php echo $data['title'] ?></title>
</head>
<body>
    <header class="container-fluid shadow-sm">
        <div class="container align-items-center d-flex justify-content-between">
            <a class ="d-block text-secondary h1" href ="index.php?route=accueil"><span class="fw-bold">Ticket</span>Immobilier</a>
                <nav>
                    <ul class="d-flex m-0 p-0 gap-2">
                        <li >
                            <a  href="index.php?route=logements">Logements</a>
                        </li>
                        <li >
                        <a  href="index.php?route=blog">Blog</a>
                        </li>
                        <li>
                        <a  href="index.php?route=contact">Contact</a>
                        </li>
                    </ul>
                </nav>
        </div>
    </header>
</body>