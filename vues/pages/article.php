<?php $article = $data['article'] ?>
<section class="container">
    <h1 class="text-center"><?php echo $article['titre']; ?></h1>
    <div class="article-meta d-flex justify-content-center">
        <p><?php echo $article['auteur']; ?></p>
        <p><?= $article['date']; ?></p>
    </div>
    <div class="article-img text-center">
        <img class ="img-fluid rounded shadow-sm"src="<?= $article['image'] ?>" alt="">
    </div>
    <div class="article-content">
        <p><?= $article['contenu'] ?></p>
    </div>
</section>
