<div class="container-fluid">
    <div class="row">
        <?php include('vues/admin/aside.php') ?>
        <section class="col-9">
            <h1>Ajouter un article</h1>
            <form action="#" method="POST">
                <p>
                    <label for="titre">Titre</label>
                    <input type="text" name="titre" id="titre">
                </p>
                <p>
                    <label for="auteur">Auteur</label>
                    <input type="text" name="auteur" id="auteur">
                </p>
                <p>
                    <label for="date">Date</label>
                    <input type="date" name="date" id="date">
                </p>
                <p>
                    <label for="image">Image</label>
                    <input type="text" name="image" id="image">
                </p>
                <p>
                    <label for="contenu">Contenu</label>
                    <textarea name="contenu" id="contenu"></textarea>
                </p>
                <p>
                    <input type="submit" value="Créer" class="btn btn-success">
                </p>
            </form>
        </section>
    </div>
</div>