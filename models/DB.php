<?php
    class DB{
        private $user = 'root';
        private $password = '';
        private $dbname = 'designer2023';

        protected function connect(){
            return new PDO('mysql:host=localhost;dbname='.$this->dbname,$this->user,$this->password);
        }
    }